#!/bin/bash

#tamanho em megabytes
SIZE=1000

if [ -f 1.txt ]; then rm 1.txt; fi
if [ -f 2.txt ]; then rm 2.txt; fi

echo "Tempo de processamento (com gravacao)"
for i in $(seq 1 10);
do
    echo "Iteracao $i";
    dd if=/dev/zero of=test.img bs=1 count=0 seek=$(echo -n $SIZE)M > /dev/null 2>/dev/null
    echo `(time -p openssl enc -e -aes-256-cbc -a -in test.img -k 10 > test.enc) 2>&1` | grep real >> 1.txt
    rm test.enc
done
echo -n "Tempo total medio (p/ tamanho = $SIZE): "
cat 1.txt | awk 'BEGIN{x=0;y=0}{x=x+$2;y=y+1}END{print x/y}'| head -c -1 > 3.txt
echo $(cat 3.txt)s
echo -n "Velocidade total media: "
echo "$SIZE /" $(cat 3.txt) | bc -l | head -c 10
echo "MB/s"
echo ""
echo "Tempo de processamento (redirecionando saida para /dev/null)"
for i in $(seq 1 10);
do
    echo "Iteracao $i";
    dd if=/dev/zero of=test.img bs=1 count=0 seek=$(echo -n $SIZE)M > /dev/null 2> /dev/null
    echo `(time -p openssl enc -e -aes-256-cbc -a -in test.img -k 10 > /dev/null) 2>&1` | grep real >> 2.txt
done

echo -n "Tempo total medio (p/ tamanho = $SIZE): "
cat 2.txt | awk 'BEGIN{x=0;y=0}{x=x+$2;y=y+1}END{print x/y}' | head -c -1 > 4.txt
echo $(cat 4.txt)s
echo -n "Velocidade total media: "
echo "$SIZE /" $(cat 4.txt) | bc -l | head -c 10
echo "MB/s"
echo ''
echo -n "Diferenca de tempos: "
echo $(cat 3.txt) " - " $(cat 4.txt) | bc -l
echo -n "Velocidade estimada de gravacao: "
echo "$SIZE"'/('"$(cat 3.txt)-$(cat 4.txt)"')' > 5.txt
cat 5.txt | bc -l | head -c 10
echo " MB/s"
echo ''
echo "Lista de tempos gravada em 1.txt (com gravacao) e 2.txt (sem gravacao)"

rm 3.txt 4.txt 5.txt test.img
