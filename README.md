# Programa 5 - Benchmark baseado em OpenSSL
## O que faz? Para que serve?

> OpenSSL é uma biblioteca open-source de criptografia. Nosso programa repetidamente encripta arquivos grandes (da ordem de 1GB) e mede o tempo necessário.

## Por que é bom para medir desempenho?

Que tipo de atividade ele está medindo? 
> Desempenho do processador e desempenho do disco. Pois faz cálculos matemáticos na hora de encriptar os arquivos e escreve o resultado encriptado no disco. Realizamos dois testes separados, um escrevendo no disco e um descartando a saída, para que possamos comparar as versões e fornecer uma estimativa da velocidade de escrita do disco.

## O que baixar

> https://github.com/openssl/openssl

## Como compilar/instalar

O benchmark precisa do OpenSSL instalado. Basta utilizar o gerenciador de pacotes da sua distribuição Unix-like.

O nosso script de benchmarking utiliza diversas ferramentas comuns no ambiente Unix, tais como `bash`, `bc`, `awk` e `head`. Um programa crítico é o `dd`, que é usado para gerar o arquivo vazio a ser encriptado.

## Como executar

Basta executar o script `bash.sh`.
```
## Como medir o desempenho

O desempenho é medido através do tempo real (dado em segundos), medido utilizando o comando `time`. Os tempo sys e user medem o tempo de CPU; por isso escolhemos o real, que mede o tempo gasto entre o início e o fim do programa.
A medida será feita 10 vezes, e utilizaremos estas medidas para determinar a média, o desvio padrão e a margem de erro. Também utilizaremos o tempo real medido para determinar o desempenho de disco (em MB/s), subtraindo os tempos gastos com e sem acesso a disco e dividindo pelo tamanho do arquivo gravado.

O cálculo de desvio padrão e de margem de erro foi realizado com a ajuda de um software de planilha; as médias e a estimativa de velocidade de escrita são calculados pelo script.

## Medições

### Maquina A: Laptop pessoal (Lenovo Y70)

Especificações:

> Arquitetura: x86_64

> Processador: Intel Core i7-4710HQ

> Frequência: 2.50 GHz

> Memória: 12GB 1333 MHz DDR3

> Cores: 4 (+ 4 virtuais)

> SO: Linux Mint 18

> HD: KINGSTON SV300S3 (120GB SSD)

Resultados de desempenho para a máquina A (1 iteração = 1000MB): 

| Iteração | Tempo de execução (com gravação) | Tempo de execução (com gravação) |
|:--------:|:---------------------:|:---:|
|     1    |   4.15  |  3.16 |
|     2    |   4.22  |  3.14 |
|     3    |   5.20  |  3.13 |
|     4    |   4.50  |  3.12 |
|     5    |   5.33  |  3.13 |
|     6    |   4.24  |  3.12 |
|     7    |   4.28  |  3.13 |
|     8    |   4.98  |  3.13 |
|     9    |   4.27  |  3.14 |
|    10    |   5.80  |  3.13 |

Análise estatística sobre esses dados:

|            Tempo de execução            | Média | Desvio Padrão | Margem de Erro |
|:--------------------------:|:-----:|:-------------:|----------------|
|    Com gravação | 4.697 | 0.585 | 0.185 |
| Sem gravação | 3.133 | 0.012 | 0.004 |

| Diferença de tempos média | Velocidade estimada de gravação |
|:---:|:---:|
| 1.564s | 639.386 MB/s |

### Maquina B (ssh.students.ic.unicamp.br)

Especificações:

> Arquitetura: x86_64

> Processador: Intel Core i3-2130

> Frequência: 3.40 GHz

> Memória: 8GB

> Cores: 4

> SO: Fedora 24

> HD: Desconhecido

Resultados de desempenho para a máquina A (1 iteração = 1000MB): 

| Iteração | Tempo de execução (com gravação) | Tempo de execução (com gravação) |
|:--------:|:---------------------:|:---:|
| 1 | 5.88   | 5.41 |
| 2 | 5.96   | 5.52 |
| 3 | 5.84   | 5.49 |
| 4 | 5.89   | 5.39 |
| 5 | 5.89   | 5.44 |
| 6 | 5.84   | 5.44 |
| 7 | 5.87   | 5.44 |
| 8 | 5.88   | 5.44 |
| 9 | 5.89   | 5.45 |
| 10 | 5.88  | 5.44 |

Análise estatística sobre esses dados:

|            Tempo de execução            | Média | Desvio Padrão | Margem de Erro |
|:--------------------------:|:-----:|:-------------:|----------------|
|    Com gravação | 5.882 | 0.0333 | 0.0105 |
| Sem gravação | 5.446 | 0.0366 | 0.0116 |

	
	
	

| Diferença de tempos média | Velocidade estimada de gravação |
|:---:|:---:|
| 0.436 s | 2293.578 MB/s |